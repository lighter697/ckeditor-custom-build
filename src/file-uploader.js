import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import * as filestack from 'filestack-js';

export class FileUploader extends Plugin {
	init() {
		const apiKey = this.editor.config.get('fileStackApiKey');
		if (!apiKey) {
			throw 'fileStackApiKey is not defined in editor config';
		}
		const client = filestack.init(apiKey);

		// Configure schema.
		this.editor.model.schema.register('video', {
			isObject: true,
			isBlock: true,
			allowWhere: '$block',
			allowAttributes: ['alt', 'src', 'srcset']
		});

		this.editor.model.schema.register('audio', {
			isObject: true,
			isBlock: true,
			allowWhere: '$block',
			allowAttributes: ['alt', 'src', 'srcset']
		});

		this.editor.conversion.for('downcast').elementToElement({
			model: 'video',
			view: (modelItem, viewWriter) => {

				const insertedElement = viewWriter.createContainerElement('video', {
					width: 400,
					src: modelItem.getAttribute('src'),
					controls: true
				});

				const figure = viewWriter.createContainerElement('p', {class: 'video-element'});
				viewWriter.insert(viewWriter.createPositionAt(figure, 0), insertedElement);

				return figure;
			}
		});

		this.editor.conversion.for('downcast').elementToElement({
			model: 'audio',
			view: (modelItem, viewWriter) => {
				const insertedElement = viewWriter.createContainerElement('audio', {
					width: 400,
					src: modelItem.getAttribute('src'),
					controls: true
				});

				const figure = viewWriter.createContainerElement('p', {class: 'audio-element'});
				viewWriter.insert(viewWriter.createPositionAt(figure, 0), insertedElement);

				return figure;
			}
		});

		const options = {
			onFileUploadFinished: file => {
				if (file.mimetype.indexOf('image') !== -1) {
					this.createInsertedElement('image', file.url);
				} else if (file.mimetype.indexOf('video') !== -1) {
					this.createInsertedElement('video', file.url);
				} else if (file.mimetype.indexOf('audio') !== -1) {
					this.createInsertedElement('audio', file.url);
				} else {
					alert('File type is not supported!');
				}
			},
			fromSources: ['local_file_system'],
		};

		this.editor.ui.componentFactory.add('insertImage', locale => {
			const view = new ButtonView(locale);
			view.set({
				withText: true,
				label: 'Insert Image',
			});

			// Callback executed once the image is clicked.
			view.on('execute', () => {
				client.picker(options).open();
			});

			return view;
		});

		this.editor.ui.componentFactory.add('insertVideo', locale => {
			const view = new ButtonView(locale);
			view.set({
				withText: true,
				label: 'Insert Video',
			});

			// Callback executed once the image is clicked.
			view.on('execute', () => {
				client.picker(options).open();
			});

			return view;
		});

		this.editor.ui.componentFactory.add('insertAudio', locale => {
			const view = new ButtonView(locale);
			view.set({
				withText: true,
				label: 'Insert Audio',
			});

			// Callback executed once the image is clicked.
			view.on('execute', () => {
				client.picker(options).open();
			});

			return view;
		});
	}

	createInsertedElement(tagName, src) {
		this.editor.model.change(writer => {
			// Create inserted element
			const insertedElement = writer.createElement(tagName, {src: src});
			// Insert content
			this.editor.model.insertContent(insertedElement, this.editor.model.document.selection);
		});
	}
}
